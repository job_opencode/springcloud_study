package com.doujiao.eurekadiscovery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @ClassName:EurekaDiscoveryApplication
 * @Description:eurekaDiscovery（服务提供方）
 * @Author:doujiao
 * @Create:2018-09-03 10:35
 **/
//使用@EnableDiscoveryClient注解服务提供方配置
@SpringBootApplication
@EnableDiscoveryClient
public class EurekaDiscovery1Application {
    public static void main(String[] args) {
        SpringApplication.run(EurekaDiscovery1Application.class, args);
    }
}
