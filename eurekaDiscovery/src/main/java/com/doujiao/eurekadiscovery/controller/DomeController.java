package com.doujiao.eurekadiscovery.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * @ClassName:TestController
 * @Description:在服务提供者eurekaDiscovery中开发部分接口给消费者调用
 * @Author:doujiao
 * @Create:2018-09-03 11:19
 **/
@RestController
public class DomeController {

    @Autowired
    DiscoveryClient discoveryClient;

    @GetMapping("/dc")
    public String dc() {
        String services = "Services: " + discoveryClient.getServices() + new Date().getTime();
        System.out.println(services);
        return services;
    }

    @GetMapping("/add")
    public Integer add(@RequestParam Integer a, @RequestParam Integer b) {
        System.out.println(a + b);
        return a + b;
    }


    @GetMapping("/stop")
    public String stop() throws InterruptedException {
        System.out.println("stop");
        Thread.sleep(5000L);
        return "stop";
    }

}
