package com.doujiao.eurekaserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @ClassName:EurekaServerApplication
 * @Description:eurekaServer（注册中心）
 * @Author:doujiao
 * @Create:2018-09-03 10:16
 **/
//使用@EnableEurekaServer注解开启注册中心配置
@SpringBootApplication
@EnableEurekaServer
public class EurekaServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(EurekaServerApplication.class, args);
    }
}