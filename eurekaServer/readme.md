#摘要说明：

服务注册与发现：即注册中心，微服务往往有多个服务，多个服务之间如何相互发现调用，如何进行负载均衡，如何动态更新服务；故需要注册中心，将各服务在服务中心注册，各服务之间的调用通过注册中心进行发现及调用；常见的插件为Netflix Eureka、Consul、Zookeeper、dubbo;本次选择使用Netflix Eureka。

Spring Cloud Netflix：Spring Cloud Eureka是Spring Cloud Netflix项目下的服务治理模块。而Spring Cloud Netflix项目是Spring Cloud的子项目之一，主要内容是对Netflix公司一系列开源产品的包装，它为Spring Boot应用提供了自配置的Netflix OSS整合。通过一些简单的注解，开发者就可以快速的在应用中配置一下常用模块并构建庞大的分布式系统。它主要提供的模块包括：服务发现（Eureka），断路器（Hystrix），智能路由（Zuul），客户端负载均衡（Ribbon）等。
