#摘要说明：
服务网关是微服务架构中一个不可或缺的部分。通过服务网关统一向外系统提供REST API的过程中，除了具备服务路由、均衡负载功能之外，它应还具备了权限控制等功能。


Spring Cloud Zuul:为微服务架构提供了前门保护的作用，同时将权限控制这些较重的非业务逻辑内容迁移到服务路由层面，使得服务集群主体能够具备更高的可复用性和可测试性。

测试zuul

先后启动服务：

服务注册中心（eurekaServer）：eureka-server（1001）

服务提供者（eurekaDiscovery）：eureka-client（2001）

服务消费者（ribbon）：ribbon-consumer（3001）


服务消费者（ribbon）：ribbon-consumer（3002）

服务消费者（feign）：feign-consumer（4001）


服务消费者（feign）：feign-consumer（4002）

服务消费者（hystrix）：hystrix-consumer（5001）

服务网关（zuul）：zuul（8001）


分别测试上述配置

http://localhost:8001/hystrix/test         --》返回401异常且两个过滤器都执行过


http://localhost:8001/hystrix/test?accessToken=11           --》正常返回

http://localhost:8001/ribbon/test?accessToken=11          --》正常返回


http://localhost:8001/feign-consumer/test?accessToken=11          --》正常返回


http://localhost:8001/feign-consumer/test?accessToken=zuul          --》返回401异常

