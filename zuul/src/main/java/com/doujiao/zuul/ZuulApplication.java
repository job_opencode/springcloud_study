package com.doujiao.zuul;

import com.doujiao.zuul.filter.AccessTokenFilter;
import com.doujiao.zuul.filter.AccessTokenFilter1;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;

/**
 * @ClassName:ZuulApplication
 * @Description:配置zuul
 * @Author:doujiao
 * @Create:2018-09-07 17:00
 **/
@SpringBootApplication
@EnableZuulProxy
@EnableDiscoveryClient
public class ZuulApplication {
    public static void main(String[] args) {
        SpringApplication.run(ZuulApplication.class, args);
    }
    // 配置过滤器
    @Bean
    public AccessTokenFilter accessTokenFilter() {
        return new AccessTokenFilter();
    }


    // 配置多个过滤器
    @Bean
    public AccessTokenFilter1 accessTokenFilter1() {
        return new AccessTokenFilter1();
    }

}