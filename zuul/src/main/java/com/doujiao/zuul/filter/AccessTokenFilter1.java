package com.doujiao.zuul.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;

import javax.servlet.http.HttpServletRequest;

/**
 * @ClassName:AccessTokenFilter1
 * @Description:AccessTokenFilter1设置需要添加参数accessToken且参数不能等于zuul，执行顺序为2
 * @Author:doujiao
 * @Create:2018-09-07 17:12
 **/
public class AccessTokenFilter1 extends ZuulFilter {

    @Override
    public Object run() throws ZuulException {
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();
        Object accessToken = request.getParameter("accessToken");
        System.out.println("accessToken1:" + accessToken);
        if (accessToken == null || "zuul".equals(accessToken)) {
            ctx.setSendZuulResponse(false);
            ctx.setResponseStatusCode(401);
            return null;
        }
        return null;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public int filterOrder() {
        return 2;
    }

    @Override
    public String filterType() {
        return "pre";
    }

}