#摘要说明：
Spring Cloud Turbine:当Hystrix消费者多个时需要先将各个消费者的执行信息聚合在一起再展示到展示面板。

先后启用：


服务注册中心（eurekaServer）：eureka-server（1001）

服务提供者（eurekaDiscovery）：eureka-client（2001）


服务消费者（hystrix）：hystrix-consumer（5001）

服务消费者（hystrix）：hystrix-consumer（5002）


Hystrix监控面板（hystrixDashboard）：hystrix-dashboard（6001）

监控数据集群（turbine）：turbine（7001）
