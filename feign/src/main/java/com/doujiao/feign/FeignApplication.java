package com.doujiao.feign;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @ClassName:FeignApplication
 * @Description:Feign的服务消费者
 * @Author:doujiao
 * @Create:2018-09-03 15:32
 **/

//使用@EnableDiscoveryClient和@EnableFeignClients注解开启feign的服务消费者配置；
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
public class FeignApplication {
    public static void main(String[] args) {
        SpringApplication.run(FeignApplication.class, args);
    }
}
