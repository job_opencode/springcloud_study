package com.doujiao.feign.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @ClassName:ss
 * @Description:Feign接口
 * @Author:doujiao
 * @Create:2018-09-03 15:35
 **/
//使用Feign与Ribbon不同的是需要调用前实现规范化接口，使用@FeignClient和Spring Mvc注解来绑定服务提供方的REST接口
//根据服务名称来指定服务提供方
@FeignClient("eureka-client")
public interface TestService {
    // 通过注解指定访问方式，访问路径，访问参数
    @RequestMapping(method = RequestMethod.GET, value = "/add")
    Integer add(@RequestParam(value = "a") Integer a, @RequestParam(value = "b") Integer b);


}


