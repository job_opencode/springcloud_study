package com.doujiao.feign.controller;

import com.doujiao.feign.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName:TestController
 * @Description:Feign接口
 * @Author:doujiao
 * @Create:2018-09-03 15:40
 **/
@RestController
public class TestController {

    @Autowired
    TestService testService;


    @GetMapping("/test")
    public Integer test() {
        return testService.add(12, 6);
    }

}



