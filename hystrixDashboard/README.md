摘要说明：

Spring Cloud Hystrix Dashboard：收集Hystrix执行过程中的重要指标，整理展示为可视化的数据面板以达到服务监控的作用。


先后启动：



服务注册中心（eurekaServer）：eureka-server（1001）

服务提供者（eurekaDiscovery）：eureka-client（2001）


服务消费者（hystrix）：hystrix-consumer（5001）

Hystrix监控面板（hystrixDashboard）：hystrix-dashboard（6001）


在上述主页面输入http://localhost:5001/hystrix.stream进入监控面板页；多次调用http://localhost:5001/test，http://localhost:5001/test1查看监控面板指标：
