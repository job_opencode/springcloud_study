package com.doujiao.ribbon.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @ClassName:TestController
 * @Description:在服务提供者eurekaDiscovery中开发部分接口给消费者调用
 * @Author:doujiao
 * @Create:2018-09-03 11:19
 **/
@RestController
public class TestController {

    @Autowired
    RestTemplate restTemplate;

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public String test() {
        return restTemplate.getForEntity("http://EUREKA-CLIENT/dc", String.class).getBody();
    }

}
